## Jar Request
### Project Name:
<!-- Name of the Minecraft server project -->

### Project Website/Repository:
<!-- Link to the official website of the project -->

### Download Link/Page:
<!-- Link to where users commonly download jars for this project -->

### Description:
<!-- Brief description of why this jar should be added and how it differs from jars currently included in the inventory -->
