FROM python:3.10-alpine

# Copy source
WORKDIR /bigbucket
COPY requirements.txt ./

# Dependency Build
RUN python3 -m venv ./.venv \
    && . .venv/bin/activate \
    && pip install --upgrade pip \
    && pip3 install --no-cache-dir -r requirements.txt \
    && deactivate

# Copy Source w/ perms & prepare default config from example
COPY ./ ./
RUN mv ./src/config ./src/config_original \
    && chmod +x ./entrypoint.sh

# Start using venv as the default python environment
ENTRYPOINT ["/bigbucket/entrypoint.sh"]

# Add meta labels
ARG BUILD_DATE
ARG BUILD_REF
ARG BB_VER
LABEL \
    maintainer="Zedifus <https://gitlab.com/zedifus>" \
    org.opencontainers.image.created=${BUILD_DATE} \
    org.opencontainers.image.revision=${BUILD_REF} \
    org.opencontainers.image.version=${BB_VER} \
    org.opencontainers.image.title="Big Bucket" \
    org.opencontainers.image.description="Python based Minecraft Jar Repository" \
    org.opencontainers.image.url="https://bigbucket.arcadiatech.org/" \
    org.opencontainers.image.documentation="https://docs.craftycontrol.com" \
    org.opencontainers.image.source="https://gitlab.com/crafty-controller/big-bucket" \
    org.opencontainers.image.vendor="Arcadia Technology, LLC." \
    org.opencontainers.image.licenses="GPL-3.0"
