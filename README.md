<div align="center">
  ![Big Bucket Logo](.gitlab/assets/bigbucket.png){height=200px}
  <h1>Big Bucket <small>1.0.0</small></h1>
  <code>Python based Jar Repository for Minecraft Servers.</code>
</div>

## What is Big Bucket?
Big Bucket Is a fully open-source, S3 compliant, Jar Repository/Cache for Minecraft Servers. (Spawned from [Crafty Controller](https://gitlab.com/crafty-controller/crafty-4)'s Server Builder repository requirements)

Fully self-hostable! So, if you would like to use your own Domain w/ Cloudflare R2 / AWS S3 etc, or just want to have everything on your own network, running on a Minio S3 Cluster. This is for you!

Jars are downloaded, hashed and uploaded in a standard format to an S3 compatible bucket.
The `manifest.json` at `https://jars.example.com/manifest.json` provides the list of available jars. See `manifest.json.format` for an example of a blank manifest file.

## Documentation
Documentation available on [Coming Soon!](https://docs.craftycontrol.com)

## Meta
Project Homepage - https://bigbucket.arcadiatech.org / https://craftycontrol.com

Discord Server - https://discord.gg/9VJPhCE

## Deployment items:

Tailored for deployment using Cloudflare R2 storage and cloudflare cache/CDN.

### Environment variables:
Required:
- DISCORD_WEBHOOK_URL_PUBLIC: Discord webhook url for public jar update messages.
- DISCORD_WEBHOOK_URL_PRIVATE: Discord webhook url for private jar updates messages/errors.
- S3_ACCESS_KEY_ID: S3 bucket access key ID.
- S3_SECRET_KEY: S3 bucket secret key.
- S3_URL: S3 compatible API url.
- S3_BUCKET_NAME: S3 bucket name.
- S3_BUCKET_REGION: S3 bucket region ('auto' for Cloudflare r2).
- HOST_URL: Final deployment url for jar service Ex: `https://jars.example.com/`.

Optional if Cloudflare Cache should be purged.
- CLOUDFLARE_ZONE_ID: Zone identifer for site in Cloudflare, used for cache purge API
- CLOUDFLARE_API_TOKEN: API token for cloudflare cache purge API.

### Config.json
- vanilla_manifest_url: Microsoft/Mojang official jars json manifest. Default: `https://launchermeta.mojang.com/mc/game/version_manifest.json`
- paper_api_url: URL to paper API. Default: `https://api.papermc.io/v2/`
- fabric_downloads_url: Fabric downloads base URL. Default: `https://meta.fabricmc.net/v2/versions/loader/`
- forge_downloads_url: Forge downloads base URL. Default: `https://maven.minecraftforge.net/net/minecraftforge/forge/`
- bungee_cord_download_url: Direct link to build of Bungee-cord to download. Default is the latest stable build: `https://ci.md-5.net/job/BungeeCord/lastStableBuild/artifact/bootstrap/target/BungeeCord.jar`
- purpur_downloads_url: Base download url for Purpur. Default: `https://api.purpurmc.org/v2/purpur/`
- fabric_loader_version: Fabric loader version to download.
- fabric_installer_version: Fabric installer version to download.

### Minecraft Forge
Due to different versions of Forge being available for different versions of Minecraft, we need information about what version of forge to get for a specific version of Minecraft.

This is done with the `forge-versions.yml` file. Each line represents a version of Minecraft and the corresponding version of Forge that should be downloaded. This file should be updated regularly to included new updated versions of forge.

### Recommended Cloudflare settings for deployment
By default, JAR files are cached by Cloudflare, JSON files are not.
- Tiered cache enabled
- Cache rule to cache all files in domain or subdomain
- redirect rule for root of domain or subdomain to a readme file or status page.
- Additional files recommended in bucket:
  - `/healthcheck` file for clients.
  - `/.well-known/security.txt` for [security reporting](https://securitytxt.org).
  - `/README.txt` contains information for users viewing bucket files directly.
