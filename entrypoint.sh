#!/bin/sh
cat << EOF
  ____  _             ____             _        _
 | __ )(_) __ _      | __ ) _   _  ___| | _____| |_
 |  _ \| |/ _\` |_____|  _ \| | | |/ __| |/ / _ \ __|
 | |_) | | (_| |_____| |_) | |_| | (__|   <  __/ |_
 |____/|_|\__, |     |____/ \__,_|\___|_|\_\___|\__|
          |___/
                             By Arcadia Technology, LLC

EOF

# Check if config exists taking one from image if needed.
if [ ! "$(ls -A ./src/config | grep -v '^\.gitkeep$')" ]; then
    printf "\033[36mWrapper | \033[33m🏗️  Config not found, pulling defaults...\033[0m\n"
    mkdir -p ./src/config/ 2> /dev/null
    cp -r ./src/config_original/* ./src/config/
    printf "\033[36mWrapper | \033[32m✅ Initialization complete!\033[0m\n"
fi

printf "\033[36mWrapper | \033[32m🚀 Launching Bigbucket with [\033[34m%s\033[32m]\033[0m\n" "$@"
source ./.venv/bin/activate
exec python3 -u src/main.py "$@"

exit
