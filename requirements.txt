boto3==1.34.89
botocore==1.34.89
discord-webhook==1.3.1
deepdiff==7.0.1
humanize==4.9.0
PyYAML==6.0.1
requests==2.31.0
