"""
BigBucket

For downloading Minecraft server and proxy jar files, hashing them,
and uploading them to an S3 bucket, for consumption by the Crafty Client.
"""

import shutil
import sys
import time
import json
import os
import hashlib
import datetime
from pathlib import Path

import boto3
import requests
import yaml
import humanize

from botocore.exceptions import ClientError
from discord_webhook import DiscordWebhook
from deepdiff import DeepDiff, Delta

# Define the base directory structure
BASE_DIR = Path(__file__).parent
CONFIG_PATH = BASE_DIR / "config"
OUTPUT_PATH = BASE_DIR / "output"
DOWNLOAD_PATH = OUTPUT_PATH / "downloads"
MSG_TEMPLATES_PATH = CONFIG_PATH / "message_templates"

# Define configuration file and an output files
config_file = CONFIG_PATH / "config.json"
forge_versions_file = CONFIG_PATH / "forge-versions.yml"
manifest_format_file = CONFIG_PATH / "manifest.json.format"
manifest_file = OUTPUT_PATH / "manifest.json"
manifest_hash_file = OUTPUT_PATH / "manifest.json.sha256"


def get_paper_project_versions(paper_url: str, project: str) -> [str] or None:
    time.sleep(1)
    request_url = f"{paper_url}projects/{project}"
    resp = requests.get(request_url, timeout=30)

    if resp.status_code == 200:
        response_data = resp.json()
        return reversed(response_data["versions"])
    else:
        return None


def get_latest_build_for_version(
    paper_url: str, project: str, jar_version: str
) -> (str or None, str or None):
    # Anti rate-limit
    time.sleep(1)
    request_url = f"{paper_url}projects/{project}/versions/{jar_version}"
    resp = requests.get(request_url, timeout=30)
    response_data = resp.json()
    if resp.status_code != 200:
        return None, None
    builds = response_data["builds"]
    latest_build = max(builds)

    request_url = (
        f"{paper_url}projects/{project}/versions/{jar_version}/builds/{latest_build}"
    )
    resp = requests.get(request_url, timeout=30)
    if resp.status_code != 200:
        return None, None
    response_data = resp.json()
    build_name = response_data["downloads"]["application"]["name"]
    build_hash = response_data["downloads"]["application"]["sha256"]
    build_url = (
        f"{paper_url}projects/{project}/versions/{jar_version}/builds/"
        f"{latest_build}/downloads/{build_name}"
    )
    return build_hash, build_url


def get_vanilla_versions(vanilla_manifest: str) -> [(str, str)]:
    """
    :param vanilla_manifest: JSON Manifest URL for vanilla servers
    :return: list of (version name, version url)

    Returns of list of vanilla versions in vanilla manifest. Stops at version 1.10.
    """
    # Anti rate-limit
    time.sleep(1)

    # Get version list from manifest
    resp = requests.get(vanilla_manifest, timeout=30)
    response_data = resp.json()
    versions = response_data["versions"]

    # Create list that will be returned
    versions_list = []
    for jar_ver in versions:
        if jar_ver["type"] == "release":
            try:
                # Removes non-standard formatted versions like 1.x pre-2, 1.x rc-1
                # Remove versions lower than 1.10
                version_minor = jar_ver["id"].split(".")[1]
            except KeyError:
                continue
            if int(version_minor) >= 10:
                # Create list of tuples (version_name, version download url)
                versions_list.append((jar_ver["id"], jar_ver["url"]))
    return versions_list


def get_vanilla_build_for_version(version_url: str) -> (str or None, str or None):
    """
    :param version_url: URL to version's JSON manifest
    :return: Returns the download url for the server jar of the requested version None if not found
    """
    # Anti rate-limit
    time.sleep(1)
    resp = requests.get(version_url, timeout=30)
    response_data = resp.json()
    # Check server jar is available, otherwise return None
    if "server" not in response_data["downloads"]:
        return None, None
    # Return download url for server version
    return (
        response_data["downloads"]["server"]["url"],
        response_data["downloads"]["server"]["sha1"],
    )


def download_and_hash_from_url(
    ver_id: str, dl_url: str, ver_name: str, host_url: str
) -> (str or None, str or None, str or None):
    # Setup download url
    download = requests.get(dl_url, timeout=30)

    # Break out of URL is not available
    if download.status_code != 200:
        return None, None, None

    # Define download location
    download_path = DOWNLOAD_PATH / f"{ver_name}/{ver_id}/{ver_name}.jar"

    # Make sure the directory exists (create if not)
    download_path.parent.mkdir(parents=True, exist_ok=True)

    # Write to the file (No need to .close() boat, the with handles that)
    with download_path.open("wb+") as downloaded_file:
        downloaded_file.write(download.content)

    version_hash_sha1, version_hash_sha256 = hash_file(download_path)

    # Return download url and file hash
    return (
        f"{host_url}{ver_name}/{ver_id}/{ver_name}.jar",
        version_hash_sha1,
        version_hash_sha256,
    )


def key_name_from_file_url(req_url: str, host_url: str) -> str:
    """
    :param url: URL to target file
    :param host_url: Base URL of jar delivery system
    :return: Returns the key name of the file, does not include downloads folder
    """
    return req_url[len(host_url) :]


def get_string_list_from_delta_string(start_string: str) -> [str]:
    """
    :param start_string: Diff string from deepdiff.
       Ex: root['mc_java_servers']['vanilla']['versions']['1.20.4']
    :return: Returns list of strings to dict path.
       Ex: ['mc_java_servers', vanilla', 'versions', '1.20.4']
    """
    start_string = start_string.strip("root['")
    export_list = start_string.split("']['")
    export_list[len(export_list) - 1] = export_list[len(export_list) - 1].strip("']")
    return export_list


def cleanup_and_kill(**kwargs):
    error_message = kwargs.get("error_message", None)
    msg_vars = kwargs.get("message_vars", None)
    discord_webhook = kwargs.get("discord_webhook")
    if error_message is not None and discord_webhook is not None:
        error_message_template_path = MSG_TEMPLATES_PATH / f"{error_message}.txt"

        with error_message_template_path.open("r") as template:
            error_message_contents = template.read()

        if msg_vars is not None:
            message = error_message_contents.format(**msg_vars)
        else:
            message = error_message_contents
        webhook_kill = DiscordWebhook(content=message, url=discord_webhook)
        _ = webhook_kill.execute()

    print("Cleaning up files")
    try:
        shutil.rmtree(DOWNLOAD_PATH)
        os.remove(manifest_file)
        os.remove(manifest_hash_file)
    except FileNotFoundError:
        print("Cleanup did not complete successfully!")
        sys.exit(1)
    sys.exit(0)


def hash_file(path: str) -> (str, str):
    """
    :param file_path: Path to file to hash
    :return: sha1 and sha256 hash of the file
    """
    sha256 = hashlib.sha256()
    sha1 = hashlib.sha1()
    with open(path, "rb") as hashed_file:
        while True:
            data = hashed_file.read(65536)
            if not data:
                break
            sha256.update(data)
            sha1.update(data)
        hashed_file.close()
    file_sha256 = sha256.hexdigest()
    file_sha1 = sha1.hexdigest()
    return file_sha1, file_sha256


def get_empty_manifest_from_file(manifest_format_file_path: Path) -> dict:
    with manifest_format_file_path.open("r") as empty_manifest:
        try:
            return json.load(empty_manifest)
        except json.decoder.JSONDecodeError:
            cleanup_and_kill(
                error_message="unable_to_load_config",
                discord_webhook=DISCORD_WEBHOOK_URL_PRIVATE,
            )


time_execution_start = time.time()
"""
Manifest base creation
Schema:
manifest_version: 1
mc_java_servers:
    Example server type:
        versions:
            1.1.example:
                url: list of jar url(s)
                sha256: sha256 hash of jar
            1.2.example:
                ...
mc_java_proxies:
    Example proxy type:
        versions:
            1.1.example:
                url: list of jar url(s)
                sha256: sha256 hash of jar
            1.2.example:
                ...
"""

manifest = get_empty_manifest_from_file(manifest_format_file)


# CONSTANTS #
#############
# VANILLA_URL: Manifest url for vanilla file versions.
# PAPER_API: Paper API url, currently on version 2
# HOST_URL: Host URL for finished manifest/downloads
# FABRIC_URL: Fabric base URL
# LATEST_FABRIC_LOADER: Latest Fabric loader version for fabric downloads
# LATEST_FABRIC_INSTALLER: Latest Fabric installer version for fabric downloads
# FORGE_URL: Base forge URL
# LATEST_FORGE_VERSION: Latest forge version for forge installer
# BUNGEE_CORD_URL: Direct download url for latest successful Bungee cord build
# DISCORD_WEBHOOK_URL: URL for discord webhook for completion messages
# S3_BUCKET_NAME: The name of the s3 target bucket

# Required
DISCORD_WEBHOOK_URL_PUBLIC = os.environ.get("DISCORD_WEBHOOK_URL_PUBLIC")
DISCORD_WEBHOOK_URL_PRIVATE = os.environ.get("DISCORD_WEBHOOK_URL_PRIVATE")
S3_ACCESS_KEY = os.environ.get("S3_ACCESS_KEY_ID")
S3_SECRET_KEY = os.environ.get("S3_SECRET_KEY")
S3_ENDPOINT = os.environ.get("S3_URL")
S3_BUCKET_NAME = os.environ.get("S3_BUCKET_NAME")
S3_BUCKET_REGION = os.environ.get("S3_BUCKET_REGION")
HOST_URL = os.environ.get("HOST_URL")
# Optional
CLOUDFLARE_ZONE_ID = os.environ.get("CLOUDFLARE_ZONE_ID")
CLOUDFLARE_API_TOKEN = os.environ.get("CLOUDFLARE_API_TOKEN")
# Check if the URL is not None and does not end with a slash, then append one
if HOST_URL and not HOST_URL.endswith("/"):
    HOST_URL += "/"

env_list = [
    DISCORD_WEBHOOK_URL_PUBLIC,
    DISCORD_WEBHOOK_URL_PRIVATE,
    S3_ACCESS_KEY,
    S3_SECRET_KEY,
    S3_ENDPOINT,
    S3_BUCKET_NAME,
    S3_BUCKET_REGION,
    HOST_URL,
]

cloudflare_api_env_list = [
    CLOUDFLARE_ZONE_ID,
    CLOUDFLARE_API_TOKEN,
]

if None in env_list:
    print("Missing config environment variable(s), terminating...")
    sys.exit(1)

if None in cloudflare_api_env_list:
    print(
        "Optional cloudflare API environment variable(s) missing, continuing without purging cache."
    )
    PURGE_CACHE = False
else:
    PURGE_CACHE = True

# Load config.json
with config_file.open("r") as cfg_file:
    try:
        config = json.load(cfg_file)
    except json.decoder.JSONDecodeError:
        cleanup_and_kill(
            error_message="unable_to_load_config",
            discord_webhook=DISCORD_WEBHOOK_URL_PRIVATE,
        )

# Load config variables
VANILLA_URL = config["vanilla_manifest_url"]
PAPER_API = config["paper_api_url"]
FABRIC_URL = config["fabric_downloads_url"]
LATEST_FABRIC_LOADER = config["fabric_loader_version"]
LATEST_FABRIC_INSTALLER = config["fabric_installer_version"]
PURPUR_BASE_URL = config["purpur_downloads_url"]
FORGE_URL = config["forge_downloads_url"]
BUNGEE_CORD_URL = config["bungee_cord_download_url"]


# VANILLA MC #
##############
# Works based on manifest json located at VANILLA_URL
# Currently skips all snapshot versions, only release versions.
# Minimum version tested MC 1.10

# Setup empty dict for Vanilla, Fabric, and Forge
vanilla_dict = {}
fabric_dict = {}
forge_dict = {}
purpur_dict = {}

# Import forge versions yml file
with forge_versions_file.open("r") as forge_yml_file:
    try:
        forge_versions_yml = yaml.safe_load(forge_yml_file)
    except yaml.YAMLError as exc:
        print(exc)
        sys.exit(1)
print("Downloading Jar inventory")


vanilla_version_list = get_vanilla_versions(VANILLA_URL)

# Iterate through vanilla server versions
for version in vanilla_version_list:
    version_id = version[0]
    version_json_url = version[1]

    # Download vanilla jar
    download_url, sha1_hash_expected = get_vanilla_build_for_version(version_json_url)
    if download_url is not None:
        url, sha1_hash, sha256_hash = download_and_hash_from_url(
            version_id, download_url, "vanilla", HOST_URL
        )
        if sha1_hash != sha1_hash_expected:
            message_vars = {
                "offending_jar": f"Vanilla - {version_id}",
                "expected": f"{sha1_hash_expected}",
                "calculated": f"{sha1_hash}",
            }
            cleanup_and_kill(
                error_message="hash_mismatch_detected",
                discord_webhook=DISCORD_WEBHOOK_URL_PRIVATE,
                message_vars=message_vars,
            )
        vanilla_dict.update(
            {str(version_id): {"url": [str(url)], "sha256": str(sha256_hash)}}
        )
        print(f"Downloaded Vanilla  - {version_id}")

    # Download fabric jar
    # Example URL: https://meta.fabricmc.net/v2/versions/loader/1.20.4/0.15.10/1.0.1/server/jar
    fabric_download_url = (
        f"{FABRIC_URL}/{version_id}/{LATEST_FABRIC_LOADER}/"
        f"{LATEST_FABRIC_INSTALLER}/server/jar"
    )
    url, _, sha256_hash = download_and_hash_from_url(
        version_id, fabric_download_url, "fabric", HOST_URL
    )
    if url is not None and sha256_hash is not None:
        fabric_dict.update(
            {str(version_id): {"url": [str(url)], "sha256": str(sha256_hash)}}
        )
        print(f"Downloaded Fabric   - {version_id}")

    # Download Forge jar
    # Example url:
    # https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.20.2-48.1.0/forge-1.20.2-48.1.0-installer.jar
    if version_id in forge_versions_yml["versions"]:
        forge_version = forge_versions_yml["versions"][version_id]
        forge_download_link = (
            f"{FORGE_URL}{version_id}-{forge_version}/"
            f"forge-{version_id}-{forge_version}-installer.jar"
        )
        url, _, sha256_hash = download_and_hash_from_url(
            version_id, forge_download_link, "forge-installer", HOST_URL
        )
        if url is not None and sha256_hash is not None:
            forge_dict.update(
                {str(version_id): {"url": [str(url)], "sha256": str(sha256_hash)}}
            )
            print(f"Downloaded Forge    - {version_id}")

    # Download purpur version
    # Example URL: https://api.purpurmc.org/v2/purpur/1.20.4/latest/download
    purpur_download_url = f"{PURPUR_BASE_URL}{version_id}/latest/download"
    url, _, sha256_hash = download_and_hash_from_url(
        version_id, purpur_download_url, "purpur", HOST_URL
    )
    if url is not None and sha256_hash is not None:
        purpur_dict.update(
            {str(version_id): {"url": [str(url)], "sha256": str(sha256_hash)}}
        )
        print(f"Downloaded Purpur   - {version_id}")

manifest["mc_java_servers"]["types"]["vanilla"]["versions"].update(vanilla_dict)
manifest["mc_java_servers"]["types"]["fabric"]["versions"].update(fabric_dict)
manifest["mc_java_servers"]["types"]["forge-installer"]["versions"].update(forge_dict)
manifest["mc_java_servers"]["types"]["purpur"]["versions"].update(purpur_dict)


# PAPER DOWNLOADS #
###################

paper_versions = get_paper_project_versions(PAPER_API, "paper")

paper_dict = {}
for version in paper_versions:
    download_hash, download_url = get_latest_build_for_version(
        PAPER_API, "paper", version
    )
    if download_hash is None or download_url is None:
        continue
    url, _, sha256_hash = download_and_hash_from_url(
        version, download_url, "paper", HOST_URL
    )
    paper_dict.update(
        {
            f"{version}": {
                "url": [f"{HOST_URL}paper/{version}/paper.jar"],
                "sha256": f"{download_hash}",
            }
        }
    )
    if sha256_hash != download_hash:
        message_vars = {
            "offending_jar": f"Paper - {version}",
            "expected": f"{download_hash}",
            "calculated": f"{sha256_hash}",
        }
        cleanup_and_kill(
            error_message="hash_mismatch_detected",
            discord_webhook=DISCORD_WEBHOOK_URL_PRIVATE,
            message_vars=message_vars,
        )
    print(f"Downloaded Paper    - {version}")

manifest["mc_java_servers"]["types"]["paper"]["versions"].update(paper_dict)

# FOLIA DOWNLOADS #
###################

folia_versions = get_paper_project_versions(PAPER_API, "folia")
folia_dict = {}
for version in folia_versions:
    download_hash, download_url = get_latest_build_for_version(
        PAPER_API, "folia", version
    )
    if download_hash is None or download_url is None:
        continue
    url, _, sha256_hash = download_and_hash_from_url(
        version, download_url, "folia", HOST_URL
    )
    folia_dict.update(
        {
            f"{version}": {
                "url": [f"{HOST_URL}folia/{version}/folia.jar"],
                "sha256": f"{download_hash}",
            }
        }
    )
    if sha256_hash != download_hash:
        message_vars = {
            "offending_jar": f"Folia - {version}",
            "expected": f"{download_hash}",
            "calculated": f"{sha256_hash}",
        }
        cleanup_and_kill(
            error_message="hash_mismatch_detected",
            discord_webhook=DISCORD_WEBHOOK_URL_PRIVATE,
            message_vars=message_vars,
        )
    print(f"Downloaded Folia    - {version}")

manifest["mc_java_servers"]["types"]["folia"]["versions"].update(folia_dict)

# VELOCITY DOWNLOADS #
######################

folia_versions = get_paper_project_versions(PAPER_API, "velocity")
velocity_dict = {}
for version in folia_versions:
    download_hash, download_url = get_latest_build_for_version(
        PAPER_API, "velocity", version
    )
    if download_hash is None or download_url is None:
        continue
    url, _, sha256_hash = download_and_hash_from_url(
        version, download_url, "velocity", HOST_URL
    )
    velocity_dict.update(
        {
            f"{version}": {
                "url": [f"{HOST_URL}velocity/{version}/velocity.jar"],
                "sha256": f"{download_hash}",
            }
        }
    )
    if sha256_hash != download_hash:
        message_vars = {
            "offending_jar": f"Velocity - {version}",
            "expected": f"{download_hash}",
            "calculated": f"{sha256_hash}",
        }
        cleanup_and_kill(
            error_message="hash_mismatch_detected",
            discord_webhook=DISCORD_WEBHOOK_URL_PRIVATE,
            message_vars=message_vars,
        )
    print(f"Downloaded Velocity - {version}")

manifest["mc_java_proxies"]["types"]["velocity"]["versions"].update(velocity_dict)


# BUNGEE-CORD PROXY #
#####################
# Downloads latest version Bungee working build from Bungee

bungee_dict = {}
url, _, sha256_hash = download_and_hash_from_url(
    "latest", BUNGEE_CORD_URL, "bungee-cord", HOST_URL
)
if url is not None and sha256_hash is not None:
    bungee_dict.update({"latest": {"url": [str(url)], "sha256": str(sha256_hash)}})
    print("Downloaded Bungee   - latest")

manifest["mc_java_proxies"]["types"]["bungee-cord"]["versions"].update(bungee_dict)

with manifest_file.open("w") as man_file:
    json.dump(manifest, man_file, separators=(",", ":"))

print("Inventory download finished!")

# S3 UPLOADS #
##############

session = boto3.session.Session()

s3_client = session.client(
    service_name="s3",
    aws_access_key_id=S3_ACCESS_KEY,
    aws_secret_access_key=S3_SECRET_KEY,
    endpoint_url=S3_ENDPOINT,
    region_name=S3_BUCKET_REGION,
)

# Get old manifest file to determine changes
old_manifest_url = f"{HOST_URL}manifest.json"
response = requests.get(old_manifest_url, timeout=30)

old_manifest = {}

if response.status_code == 200:
    old_manifest = response.json()
else:
    # Following difference checking code assumes manifest structure is present
    print("Unable to read old manifest, assuming none and continuing.")
    old_manifest = get_empty_manifest_from_file(manifest_format_file)

if "enabled" not in old_manifest["mc_java_servers"]:
    print(
        "Old manifest appears to be from a previous version, starting as if none exsists."
    )
    old_manifest = get_empty_manifest_from_file(manifest_format_file)

# Get differences from old manifest file to new manifest file
diff = DeepDiff(old_manifest, manifest)
manifest_delta = Delta(diff)
manifest_delta_dict = manifest_delta.to_dict()

# Number of changed jars and number of new jars in the new manifest are calculated
NUM_NEW = 0
NUM_CHANGED = 0
if "dictionary_item_added" in manifest_delta_dict:
    NUM_NEW = len(manifest_delta_dict["dictionary_item_added"])
if "values_changed" in manifest_delta_dict:
    NUM_CHANGED = len(manifest_delta_dict["values_changed"])

CHANGED_JARS_STRING = ""
NEW_JARS_STRING = ""
print("---------------------------------------")
print(f"Detected {NUM_NEW} new versions")
print(f"Detected {NUM_CHANGED} changed versions")

files_to_purge = []

# Upload new additions
if NUM_NEW > 0:
    print("Uploading new jars")
    for location_and_content in manifest_delta_dict["dictionary_item_added"].items():
        location = get_string_list_from_delta_string(location_and_content[0])
        key_name = key_name_from_file_url(location_and_content[1]["url"][0], HOST_URL)
        file_path = DOWNLOAD_PATH / f"{key_name}"
        # Location sample:
        # ['mc_java_servers', 'types', 'vanilla', 'versions', '1.20.5']
        NEW_JARS_STRING += f"\n- {location[2].title()} - {location[4]}"
        try:
            response = s3_client.upload_file(file_path, S3_BUCKET_NAME, key_name)
            files_to_purge.append(f"{HOST_URL}{key_name}")
        except ClientError as e:
            print(e)
            sys.exit(1)

if NUM_CHANGED > 0:
    print("Uploading changed jars")
    for location_and_content in manifest_delta_dict["values_changed"].items():
        location = get_string_list_from_delta_string(location_and_content[0])
        # Location example:
        # ['mc_java_proxies', 'types', 'bungee-cord', 'versions', 'latest', 'sha256']
        # Created key_name for example: "bungee-cord/latest/bungee-cord.jar"
        key_name = f"{location[2]}/{location[4]}/{location[2]}.jar"
        file_path = DOWNLOAD_PATH / f"{key_name}"
        CHANGED_JARS_STRING += f"\n- {location[2].title()} - {location[4]}"
        try:
            response = s3_client.upload_file(file_path, S3_BUCKET_NAME, key_name)
            files_to_purge.append(f"{HOST_URL}{key_name}")
        except ClientError as e:
            print(e)
            sys.exit(1)

# Upload new manifest if changes have been made
if NUM_NEW > 0 or NUM_CHANGED > 0:
    print("Uploading manifest")
    # Hash manifest and create manifest.json.sha256 file
    _, manifest_hash = hash_file(manifest_file)
    with manifest_hash_file.open("w") as man_hash_file:
        man_hash_file.write(manifest_hash)

    try:
        _ = s3_client.upload_file(manifest_file, S3_BUCKET_NAME, manifest_file.name)
        _ = s3_client.upload_file(
            manifest_hash_file, S3_BUCKET_NAME, manifest_hash_file.name
        )
        files_to_purge.append(f"{HOST_URL}{manifest_file.name}")
        files_to_purge.append(f"{HOST_URL}{manifest_hash_file.name}")
    except ClientError as e:
        print(e)
        sys.exit(1)

# PURGE CLOUDFLARE CACHE #
##########################

if PURGE_CACHE:
    print("Purging cloudflare cache of updated files.")
    if len(files_to_purge) > 500:
        purge_count_too_high_message = """:warning: :warning:
    number of URLS to purge in cache is higher than 500.
    Continuing with file update but recommend manually purging entire cache."""
        webhook = DiscordWebhook(
            content=purge_count_too_high_message, url=DISCORD_WEBHOOK_URL_PUBLIC
        )
        request = webhook.execute()
    else:
        cloudflare_api_endpoint = f"https://api.cloudflare.com/client/v4/zones/{CLOUDFLARE_ZONE_ID}/purge_cache"
        data = json.dumps({"files": files_to_purge})
        headers = {"Authorization": f"Bearer {CLOUDFLARE_API_TOKEN}"}
        if NUM_NEW > 0 or NUM_CHANGED > 0:
            response = requests.post(
                cloudflare_api_endpoint, data=data, headers=headers, timeout=30
            )
            if response.status_code != 200:
                cleanup_and_kill(
                    discord_webhook=DISCORD_WEBHOOK_URL_PRIVATE,
                    error_message="cloudflare_purge_api_failure",
                )

# COMPLETION MESSAGE #
######################

print("Sending webhook message")

# Calculate downloads folder size
DOWNLOADS_DIR_SIZE = 0
for file in DOWNLOAD_PATH.rglob("*"):
    if file.is_file():  # Ensure it's a file
        DOWNLOADS_DIR_SIZE += file.stat().st_size

# Calculate execution time
execution_time = time.time() - time_execution_start

if NUM_NEW > 0:
    NEW_JARS_STRING = f"New jars added:{NEW_JARS_STRING}\n"
if NUM_CHANGED > 0:
    CHANGED_JARS_STRING = f"Updated jars:{CHANGED_JARS_STRING}\n"

public_message_vars = {
    "jars_added": NEW_JARS_STRING,
    "jars_updated": CHANGED_JARS_STRING,
}

discord_public_status_message_template_path = (
    MSG_TEMPLATES_PATH / "public_jar_update.txt"
)

with discord_public_status_message_template_path.open("r") as file:
    webhook_message = file.read()

webhook_message = webhook_message.format(**public_message_vars)

# Discord messages have a max size of 2000, send private error if public message is too large.
if len(webhook_message) > 2000:
    cleanup_and_kill(
        discord_webhook=DISCORD_WEBHOOK_URL_PRIVATE,
        error_message="public_message_too_large",
    )

private_discord_webhook_vars = {
    "execution_time": humanize.naturaldelta(datetime.timedelta(seconds=execution_time)),
    "jar_size": humanize.naturalsize(DOWNLOADS_DIR_SIZE),
}

# Send public webhook message
if NUM_NEW > 0 or NUM_CHANGED > 0:
    webhook = DiscordWebhook(content=webhook_message, url=DISCORD_WEBHOOK_URL_PUBLIC)
    request = webhook.execute()
    cleanup_and_kill(
        error_message="private_jar_update",
        discord_webhook=DISCORD_WEBHOOK_URL_PRIVATE,
        message_vars=private_discord_webhook_vars,
    )
else:
    cleanup_and_kill(
        error_message="execution_complete_no_updates",
        discord_webhook=DISCORD_WEBHOOK_URL_PRIVATE,
    )

# File cleanup #
################
cleanup_and_kill()
